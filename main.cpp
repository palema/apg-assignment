#include <exception>
using namespace std;
#include <iostream>
#include <stdio.h>
#include <fstream>
#include <string>
#include <list>

class Admin;
class Empolyee;
class Time;
class Manager;

//TIME CLASS
class Time
{
public:
	int hours;
	int minutes;
	int seconds;

	Time()
	{
	}

	Time(int hr, int min, int sec)
	{
		hours = hr;
		minutes = min;
		seconds = sec;
	};

	// copy constructor
	Time(const Time &t2)
	{
		hours = t2.hours;
		minutes = t2.minutes;
		seconds = t2.seconds;
	}

	//SETTERS

public:
	void setHours(int hr)
	{
		hours = hr;
	}

public:
	void setMinutes(int min)
	{
		minutes = min;
	}

public:
	void setSeconds(int sec)
	{
		seconds = sec;
	}
	//GETTERS

	int getHours()
	{
		return hours;
	}

	int getMinutes()
	{
		return minutes;
	}

	int getSeconds()
	{
		return seconds;
	}

public:
	Time getCurrentDateTime()
	{
		int hr, min, sec;

		const time_t now = time(nullptr);
		const tm calendar_time = *localtime(addressof(now));

		hr = calendar_time.tm_hour;
		min = calendar_time.tm_min;
		sec = calendar_time.tm_sec;

		Time time(hr, min, sec);
		return time;
	};

	// operating overloading
	Time operator - (Time& time )
	{
		Time tmp;
		tmp.hours = hours - time.hours;
		tmp.minutes = minutes - time.minutes;
		tmp.seconds = seconds - time.seconds;

		return tmp;
	}
};

//working space
///////////////////////////////////////////////////////////////
class Workspace 
{
public:
	string employeeID;
	Time startingTime;
	Time endTime;
// workspace constructors
public:
	Workspace(){};
	Workspace(string employeeID, Time startingTime, Time endTime)
	{
		employeeID = employeeID;
		startingTime = startingTime;
		endTime = endTime;
	};
// function for getting user worked hours in a day
public: 
 int getTimeWorked(string user)
 {
     string username;
	 int hours;
	 int minutes;
	 int seconds;

	  ifstream dat;
		std::string str ;
		dat.open("WorkedTime.txt");
		while (std::getline(dat, str) >> username >> hours >> minutes >> seconds)
		{
             if( user == username) {
				 return	hours;
		}       
		}
		dat.close();
 }

// function for getting salary rate of worker per hour
public:
int CalculateSalary(string user)
{
	string username;
	string employeeID;
	string jobTitle;
	int salary;

	ifstream data;
		std::string str ;
		data.open("Userdetails.txt");
		while (std::getline(data, str) >> username >> employeeID >> jobTitle>> salary)
		{
             if( user == username) {
				 return	salary;
		}       
		}
		data.close();

}

//function for getting timestamp when user logged in
public:
	Time calculateTime(string user)
	{   
		string username;
		string employeeID;
		int startingHours;
		int startingMinutes ;
		int startingSeconds ;

		ifstream data;
		std::string str ;
		data.open("dataLogin.txt");
		while (std::getline(data, str) >> username >> employeeID >> startingHours >> startingMinutes >> startingSeconds)
		{
             if( user == username) {
				 Time time(startingHours,startingHours,0);
				
				 return	time;
		}       
		}
		data.close();
		
	};
};


// Template class person
//////////////////////////////////////////////////
template <class A ,class B>
class Person{
	A name;
	A surname;
	B age;

	public:
	Person(){};
	Person(A name , A surname , B age)
	{

		name = name;
		surname = surname;
		age = age;
	}

		void setName(A name);
		void setSurname(A name);
		void setAge(B age);

		A getName();
		A getSurname();
		B getAge();
	

};

// template method
//////////////////////////////////////////////////
template <class A , class B>
void  Person<A,B>::setName(A name)
{
 name = name;
}

template <class A , class B>
void Person<A,B>::setSurname(A surname)
{
surname = surname;
}

template <class A , class B>
void Person<A,B>::setAge(B age)
{
	age = age;
};


 template <class A , class B>
A Person<A,B>::getName()
{
	return name;
}

template <class A , class B>
A Person<A,B>::getSurname()
{
	return surname;
}

template <class A , class B>
B Person<A,B>::getAge()
{
	return age;
};


//CLASS Empolyee
//////////////////////////////////////////////////////
class Empolyee  :  public Person <string ,int>
{

public:
	string employeeID;
	string password;
	double salary;
	Time time;
	string jobtitle;
	int rate;
//constructors for class Employee
	Empolyee(){};
	Empolyee(string emp, double sal, Time tm, string jbTtle, int rt)
	{
		employeeID = emp;
		salary = sal;
		time = tm;
		jobtitle = jbTtle;
		rate = rt;
	}

	Empolyee(string emp, string password, string jbTtle, int rt)
	{
		employeeID = emp;
		password = password;
		jobtitle = jbTtle;
		rate = rt;
	}

//SETTERS

public:
	void setEmployeeID(int emp)
	{
		employeeID = emp;
	}

	void setPassword(string password)
	{
		password = password;
	}

	void setSalary(double sal)
	{
		salary = sal;
	}

	void setHours(Time tm)
	{
		time = tm;
	}

	void setJobTitle(string jbTtle)
	{
		jobtitle = jbTtle;
	}

	void setRate(int rt)
	{
		rate = rt;
	}

//GETTERS

	string getEmployeeID()
	{
		return employeeID;
	}

	string getPassword()
	{
		return password;
	}

	double getSalary()
	{
		return salary;
	}

	Time getTime()
	{
		return time;
	}

	string getJobTitle()
	{
		return jobtitle;
	}

	int getRate()
	{
		return rate;
	}

//METHODS 
// virtual method in class employee
public:
	virtual bool login_page()
	{

		clearScreen(); // call clear screen

		string getUsrname, getPass, getJobTitle;
		string username;
		string password;
		cout << "Please enter your username:";
		cin >> username;
		cout << "Please enter your password:";
		cin >> password;

		//validating the user credentials with the one saved in the file
		ifstream data;
		std::string str;
		bool valid = false;
		data.open("Userdetails.txt");
		while (std::getline(data, str) >> getUsrname >> getPass >> getJobTitle)
		{
			if (username == getUsrname && password == getPass)
			{
				valid = true;
				break;
			}
		}

		if (valid == true)
		{

			cout << "Authenticated !!!!";

			// adding users time into the file datalogin.txt when logging in
			Time A;
			A.getCurrentDateTime();
			ofstream file;
			file.open("dataLogin.txt", ios::app);
			file << "\n"
				 << username << "\t" << password << "\t" << A.getCurrentDateTime().hours
				 << " \t" << A.getCurrentDateTime().minutes << "\t"
				 << A.getCurrentDateTime().seconds;
			file.close();
			return true;
		}
		else
		{
			cout << "Sorry password/username do not match";
			return false;
		}
		data.close();
	}
	//STL for sorting the names in the file
	template <class T>
	void bubbleSort(T a[], int n)
	{
		for (int i = 0; i < n - 1; i++)
			for (int j = n - 1; i < j; j--)
				if (a[j] < a[j - 1])
					swap(a[j], a[j - 1]);
	}
	void SortedLogins()
	{
		string a[] = {"dataLogin.txt"};
		int n = sizeof(a) / sizeof(a[0]);

		// calls template function
		bubbleSort<string>(a, 5);

		cout << " Sorted array : ";
		for (int i = 0; i < n; i++)
			cout << a[i] << " ";
		cout << endl;
	}


	// Takes and stores users login information, username and password
	void Register() 
	{
		string username;
		string password;
		string jobtitle;
		int salaryRate;

		cout << "\nPlease create a username:";
		cin >> username;
		cin.ignore();

		cout << "Please create a password:";
		cin >> password;
		cin.ignore();

		cout << "Please select job title : ";
		cout << "\n 1.Admin";
		cout << "\n 2.Manager";
		cout << "\n 3.Worker";

		int choice;
		cout << "\nEnter choice :";
		cin >> choice;

		if (choice == 1)
		{
			jobtitle = "Admin";
		}
		else if (choice == 2)
		{
			jobtitle = "Manager";
		}
		else if (choice == 3)
		{
			jobtitle = "Worker";
		}

		//select salary rate
		cout << "Please select Salary rate per hour :";
		cout << "\n 1.$N 300";
		cout << "\n 2.$N 200";
		cout << "\n 3.$N 100";
		cout << "\n";
		int option;
		cout << "\nEnter option :";

		cin >> option;

		if (option == 1)
		{
			salaryRate = 300;
		}
		else if (option == 2)
		{
			salaryRate = 200;
		}
		else if (option == 3)
		{
			salaryRate = 100;
		}

		//adding  user into user credential file
		ofstream data;
		data.open("Userdetails.txt", ios::app);

		data << "\n"
			 << username << " " << password << "  " << jobtitle << " " << salaryRate;
		data.close();
		cout << "Information saved\n" << endl;
		cout <<"------------------------------\n";
		
	}

	//function for logging out of the system
	void LogOut()
	{
		string getPass, getUser;

		string username;
		string password;
		cout << "Please enter your username:";
		cin >> username;
		cout << "Please enter your password:";
		cin >> password;

		//validating the user credentials with the ones in the file
		ifstream data;
		bool valid = false;
		string str;
		data.open("dataLogin.txt");
		while (std::getline(data, str) >> getUser >> getPass)
		{
			if (username == getUser && password == getPass)
			{
				valid = true;
				break;
			}
		}

		if (valid)
		{

			cout << "You have been logged out of the System";
			// adding users time details when logging out
			Time A;
			A.getCurrentDateTime();
			ofstream file;
			file.open("dataLogout.txt", ios::app);
			file << "\n"
				 << getUser << "\t" << A.getCurrentDateTime().hours << " " << A.getCurrentDateTime().minutes << "  " << A.getCurrentDateTime().seconds;
			file.close();

			//worked time
			Workspace w;
			Time ti = w.calculateTime(getUser);
			Time t2(A.getCurrentDateTime().hours,A.getCurrentDateTime().minutes,9);
			Time t3 = t2-ti ;

			//working salary
			int time = w.getTimeWorked(getUser);
			int salary = w.CalculateSalary(getUser);
			double totalSalary = time * salary;
			
			//writting user worked time into a file
			ofstream fl;
			fl.open("WorkedTime.txt", ios::app);
			fl << "\n"
				 << getUser << "\t"<< t3.hours << " " << t3.minutes << "  " << t3.seconds;
			fl.close();

			//writting calculated day salary into a file
		    ofstream data;
			data.open("DaySalary.txt", ios::app);
			data << "\n"
				 << getUser << "\t"<<totalSalary ;
			data.close();

		}
		else
		{
		cout << "We are very  sory,password/username do not match";
		}
		data.close();
	}

	void clearScreen() //function to clear the screen
	{
		cout << string(2, '\n');
	}
	
	//Employee landing menu
	void EmployeeMenu()
	{
		cout << "\n\nWelcome to employee menu";
		cout << "\n-----------------------";
		cout << "\n 1.Login";
		cout << "\n 2.Logout\n";

		int option;
		cout << "Enter your choice :";
		cin >> option;

		switch (option)
		{
		case 1:
			login_page();
			break;
		case 2:
			LogOut();
			break;
		default:
			cout << "sorry wrong option";
			break;
		}
	}
};

// Class Manager inheriting Employeee
/////////////////////////////////////////////////////////
class Manager : public Empolyee
{


public:
	Manager(){};

// overiding the vertual method that is in Employee class
public:
	bool login_page() override
	{

		clearScreen(); // call clear screen

		string getUsrname, getPass, getJobTitle;
		string username;
		string password;
		cout << "Please enter your username:";
		cin >> username;
		cout << "Please enter your password:";
		cin >> password;

		ifstream data;
		std::string str;
        
		// authenticating user
		bool valid = false;
		data.open("Userdetails.txt");
		while (std::getline(data, str) >> getUsrname >> getPass >> getJobTitle)
		{
			if (username == getUsrname && password == getPass && getJobTitle=="Manager")
			{
				valid = true;
				break;
			}
		}

		if (valid == true)
		{

			cout << "Authenticated !!!!";

			// adding users details when logging in
			Time A;
			A.getCurrentDateTime();
			ofstream file;
			file.open("dataLogin.txt", ios::app);
			file << "\n"
				 << username << "\t" << password << "\t" << A.getCurrentDateTime().hours
				 << " \t" << A.getCurrentDateTime().minutes << "\t"
				 << A.getCurrentDateTime().seconds;
			file.close();
			return true;
		}
		else
		{
			cout << "Sorry password/username do not match";
			return false;
		}
		data.close();
	}
//adding a task for a user
public:
	void addTask()
	{
		string username;
		string taskName;
		cout << "\n\nYou about to add Task :\n";
		cout<< "-----------------------------\n";
		cout << "\nEnter employee name :";
		cin >> username;
		cout << "\nEnter task name  :";
		cin >> taskName;
		ofstream file;
		file.open("Task.txt", ios::app);
		file << "\n"
			 << username << " " << taskName;
		file.close();

		cout <<"------------------------------\n";
		cout <<"\n1.Add another Task";
		cout <<"\n0.Back";
		cout << "\n------------------------------\n";
		int op;
		cout <<"Enter choice :";
		cin>>op;
		if(op==1){addTask();}
		else{managerLandingPage();}


	};
//adding overtime hours for user
public:
	void addOvertime()
	{
		string username;
		int hours;
		int minutes;
		cout << "\n\nYou are about to add overtime hours:\n";
		cout << "-----------------------------------------\n";
		cout <<"\nEnter the username :";
		cin >> username;
		cout << "\nEnter overworked hours :";
		cin >> hours;
		cout << "\nEnter overworked minutes :";
		cin >> minutes;

		ofstream file;
		file.open("OverTime.txt", ios::app);
		file << "\n"
			 << username << " " << hours << " " << minutes;
		file.close();

		cout <<"------------------------------\n";
		cout <<"\n1.Add another user's Time";
		cout <<"\n0.Back";
		cout << "\n------------------------------\n";
		int op;
		cout <<"Enter choice :";
		cin>>op;
		if(op==1){addOvertime();}
		else{managerLandingPage();}
	};

//removing task for a specific user
public:
	void removeTask(string username)
	{

		string user;
		int hours;
		int minutes;
		int salaryRate;
		cout << "\n Enter user name to remove task :";
		cin >> user;
		ifstream data;
		data.open("Task.txt");
		ofstream e;
		e.open("new.txt");
		data >> username;
		data >> hours;
		data >> minutes;

		while (!data.eof())
		{
			if (username != user)
			{
				e << endl;
				e << username;
				e << endl;
				e << hours;
				e << endl;
				e << minutes;
				e << endl;
			}

			else
			{
				cout << "\nThe Task deleted";
			}
			data >> username;
			data >> hours;
			data >> minutes;
		}
		e.close();
		data.close();
		remove("Task.txt");
		rename("new.txt", "Task.txt");
	};

//method for viewing all users
public:
	void ViewEmployee()
	{
        cout<< "\nYou are viewing employees :";
		cout<<"\n------------------------------";
		string username, user;
		string password;
		string jobtitle;
		int salaryRate;
		ifstream data;
		data.open("Userdetails.txt");
		     

		while (!data.eof())
		{
			data >> username;
			data >> password;
			data >> jobtitle;
			data >> salaryRate;
			cout<< "\n"<< username <<"\t" << password <<"\t"<< jobtitle <<"\t"<< salaryRate;
			
		}
		cout<<"\n------------------------------";
		int op;
		cout<<"\n\n0.Back :";
		cin>>op;
		if(op==0){managerLandingPage();}
		else{ViewEmployee();}
		

	}

// method for the Manager log in page
public:
	void managerLandingPage()
	{
		cout << "\nWelcome to Manager menu";
		cout << "\n------------------------";
		cout << "\n 1.View employee ";
		cout << "\n 2.Add task";
		cout << "\n 3.Add overtime";
		cout << "\n 0.Logout";
		cout << "\n------------------------";

		int choice;
		cout << "\n\nSelect your choice from above :";
		cin >> choice;
		switch ((choice))
		{
		case 1:
			ViewEmployee();
			break;
		case 2:
			addTask();
			break;
		case 3:
			addOvertime();
			break;
		case 0:
		    LogOut();
			break;
		default:
			cout<< " Sorry wrong option !!!";
			break;
		}
	};
};

//class system admin  inheriting employeee
////////////////////////////////////////////////
class Admin : public Empolyee
{
	//Overiding the virtual login page method in emmployee
	public:
	bool login_page() override
	{

		clearScreen(); // call clear screen

		string getUsrname, getPass, getJobTitle;
		string username;
		string password;
		cout << "Please enter your username:";
		cin >> username;
		cout << "Please enter your password:";
		cin >> password;

		ifstream data;
		std::string str;

		bool valid = false;
		data.open("Userdetails.txt");
		while (std::getline(data, str) >> getUsrname >> getPass >> getJobTitle)
		{
			if (username == getUsrname && password == getPass && getJobTitle=="Admin")
			{
				valid = true;
				break;
			}
		}

		if (valid == true)
		{

			cout << "Authenticated !!!!";

			// adding users details when logging in
			Time A;
			A.getCurrentDateTime();
			ofstream file;
			file.open("dataLogin.txt", ios::app);
			file << "\n"
				 << username << "\t" << password << "\t" << A.getCurrentDateTime().hours
				 << " \t" << A.getCurrentDateTime().minutes << "\t"
				 << A.getCurrentDateTime().seconds;
			file.close();
			return true;
		}
		else
		{
			cout << "Sorry password/username do not match";
			return false;
		}
		data.close();
	}
//empty Admin constructor
public:
	Admin(){};

//Registering users
public:
	void addUser()
	{
		Register();

		int op;
		cout << "\n1.Register anather user";
		cout << "\n0.Back";
		cin>>op;
		if(op==1){addUser();}
		else{AdminLandingPage();}
	};
//Removing user from the system
public:
	void removeUser()
	{
		string username, user;
		string password;
		string jobtitle;
		int salaryRate;
		cout <<"\n You are about to remove User :";
		cout <<"\n-----------------------------------";
		cout << "\n Enter username to remove :";
		cin >> user;
		ifstream data;
		data.open("Userdetails.txt");
		ofstream e;
		e.open("new.txt");
		data >> username;
		data >> password;
		data >> jobtitle;
		data >> salaryRate;
		while (!data.eof())
		{
			if (username != user)
			{
				
				e << username <<"\t"<< password <<"\t"<< jobtitle << "\t" << salaryRate <<"\n";
				
			}
			else
			{
				cout << "\nThe user deleted";
			}
			data >> username;
			data >> password;
			data >> jobtitle;
			data >> salaryRate;
		}
		e.close();
		data.close();
		remove("Userdetails.txt");
		rename("new.txt", "Userdetails.txt");
		cout << "\n-------------------------------\n";
		int opt;
		cout <<"1.Delete another User :\n";
		cout <<"0.Back";
		cout << "---------------------------------\n";
		cout <<" Enter your option :";
		cin >> opt;
		if(opt==1){removeUser();}
		else{AdminLandingPage();}
	
	};

//Editing user details
public:
	void editUser()
	{
		string username, user;
		string password;
		string jobtitle;
		int salaryRate;
		cout << "\n Enter user name to edit:";
		cin >> user;
		ifstream data;
		data.open("Userdetails.txt");
		ofstream e;
		e.open("new.txt");
		data >> username;
		data >> password;
		data >> jobtitle;
		data >> salaryRate;
		while (!data.eof())
		{
			if (username != user)
			{
				e << username <<"\t"<< password  <<"\t"<< jobtitle <<"\t"<< salaryRate <<"\n";
			}
			else
			{
				cout << "\n Enter new username :";
				cin >> username;
				cout << "\n Enter new password :";
				cin >> password;
				cout << "\n Enter new job title :";
				cin >> jobtitle;
				cout << "\n Enter new salary rate :";
				cin >> salaryRate;

				
				e << username << "\t" << password << "\t"<< jobtitle << "\t"<< salaryRate<< "\n";
				cout<<"-------------------------------------\n";
				int opt;
				cout <<"1.Edit another user\n";
				cout <<"0.Back";
				cout<< "------------------------------------\n";
				cout<<"Enter choice :";
				cin>>opt;
				if(opt==0){
					AdminLandingPage();
					break;}
				else{editUser();}


				
			}
			data >> username;
			data >> password;
			data >> jobtitle;
			data >> salaryRate;
		}
		e.close();
		data.close();
		remove("Userdetails.txt");
		rename("new.txt", "Userdetails.txt");
		cout<<"-----------------------------------\n";
		AdminLandingPage();
	};
//function for Admin landing page
public:
	void AdminLandingPage()
	{

		cout << "\nWelcome to Admin home page";
		cout << "\n----------------------------";
		cout << "\n 1.Add User";
		cout << "\n 2.Remove User";
		cout << "\n 3.Edit User";
		cout << "\n 0.Logout";

		int choice;
		cout << "\nSelect your choice from above :";
		cin >> choice;
		switch ((choice))
		{
		case 1:
			addUser();
			break;
		case 2:
			removeUser();
			break;
		case 3:
			editUser();
			break;
		case 0:
			LogOut();
			break;

		default:
			cout << "Sorry wrong option !! \n";
			break;
		}
	}
};


// Driver method
////////////////////////////////////////////////////
int main()
{
	Empolyee one;
	Time time1;
	Admin admin;
	Manager manager;
	//Menus
	int choice;

	do
	{

		cout << endl
			 << "######################################\n"
			 << "#####     Welcome to Mauna      ######\n\n"
			 << " 1 - Login Manager.\n"
			 << " 2 - Login System Admin.\n"
			 << " 3 - Login Employee.\n"
			 << " 4 - Exit.\n"
			 << " ------------------------------------\n"
			 << " choose by number and press enter: ";

		cin >> choice;

		switch (choice)
		{
		case 1:
			if (manager.login_page())
			{
				manager.managerLandingPage();
			}
			else
			{
				manager.login_page();
			}
			break;

		case 2:
			if (admin.login_page())
			{
				admin.AdminLandingPage();
			}
			else
			{
				admin.login_page();
			}
			break;

		case 3:
			if (one.login_page())
			{
				one.EmployeeMenu();
			}
			else
			{
				one.login_page();
			}
			break;

		case 4:
			exit(0);
			break;

		default:
			cout << "Not a Valid Choice. \n"
				 << "Choose again.\n";
			break;
		}
	} while (choice > 0 && choice < 5);
	return 0;
}
///////////////////////////////////////////////////////
